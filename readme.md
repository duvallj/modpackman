## Git-Based Modpack Manager
---

Script to update modpacks automatically

#### To Use

Simply put the location of your `mods` folder in `pack-location.txt` and run `python update.py install`
