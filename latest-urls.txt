# fill this file with the same as downloads.txt but with links to the latest urls instead
# e.g.
thaumcraft.jar https://minecraft.curseforge.com/projects/thaumcraft/files/latest
baubles.jar https://minecraft.curseforge.com/projects/baubles/files/latest
opencomputers.jar https://minecraft.curseforge.com/projects/opencomputers/files/latest
twilightforest.jar https://minecraft.curseforge.com/projects/the-twilight-forest/files/latest
connectedtextures.jar https://minecraft.curseforge.com/projects/ctm/files/latest
